Source: python-x-wr-timezone
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Jochen Sprickerhof <jspricke@debian.org>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-setuptools,
 python3-all,
 python3-icalendar <!nocheck>,
 python3-pygments <!nocheck>,
 python3-pytest <!nocheck>,
 python3-restructuredtext-lint <!nocheck>,
Testsuite: autopkgtest-pkg-pybuild
Standards-Version: 4.7.0
Homepage: https://github.com/niccokunzmann/x-wr-timezone
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-x-wr-timezone
Vcs-Git: https://salsa.debian.org/python-team/packages/python-x-wr-timezone.git

Package: python3-x-wr-timezone
Architecture: all
Depends:
 ${python3:Depends},
 ${misc:Depends},
Description: Python module to handle non-standard X-WR-TIMEZONE icalendar property (Python 3)
 Some calendar providers introduce the non-standard X-WR-TIMEZONE parameter to
 ICS calendar files. Strict interpretations according to RFC 5545 ignore the
 X-WR-TIMEZONE parameter. This causes the times of the events to differ from
 those which make use of X-WR-TIMEZONE.
 .
 This module aims to bridge the gap by converting calendars using X-WR-TIMEZONE
 to a strict RFC 5545 calendars.
 .
 This package installs the library for Python 3.
